PedidosYa Test
=======

Architecture
=======

The Project was developed following MVVM principles. 

Third Party Libraries
=======

Alamofire - Used to handle requests to the API.


Dependencies Manager
=======

Carthage


Running The Project
=======

Download repo and should run without problems.

Add credentials in file `Path` in line 19 and 20

Requirements
=======

Xcode 10.1
Swift 4.2
Deplyment Target 12.0
