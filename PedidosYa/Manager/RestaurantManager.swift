//
//  RestaurantManager.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import CoreLocation

class RestaurantManager: NSObject {

    private enum Country: String {
        case uruguay = "1"
    }

    // MARK: - Properties

    static let shared = RestaurantManager()

    // MARK: - Life Cycle

    private override init() {
        super.init()
    }

    // MARK: - Public Methods

    func obtainRestaurants(with location: String, country: String = Country.uruguay.rawValue, completion: @escaping AppCompletion) {
        print("Location: \(location)")
        RequestManager.shared.get(path: .restaurant(point: location, country: country)) { value, error in
            guard error == nil, let json = value as? [String: Any], let data = json["data"] as? [[String: Any]] else {
                completion(nil, error)
                return
            }

            var restaurants: [Restaurant] = []

            data.forEach({ restaurantData in
                restaurants.append(Restaurant(data: restaurantData))
            })

            print("Count: ", restaurants.count)
            completion(restaurants, nil)
            return
        }
    }
    
}
