//
//  RestaurantViewModel.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit
import CoreLocation

class RestaurantViewModel: NSObject {

    // MARK: - Properties

    let restaurants: Bindable<[Restaurant]> = Bindable([])

    private var locationManager: CLLocationManager?
    private var currentLocation: CLLocation?

    // MARK: - Events

    var presentMapViewController: (() -> ())?

    // MARK: - Life Cycle

    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
    }

    // MARK: - Public Methods

    func viewDidApper() {
        setupLocation()
    }

    func didTapMapButton() {
        presentMapViewController?()
    }

    func makeMapViewController() -> UINavigationController {
        let viewController = MapViewController.instantiateFromStoryboard()
        viewController.viewModel = MapViewModel(locationManager: CLLocationManager())
        return UINavigationController(rootViewController: viewController)
    }

    // MARK: - Private Methods

    private func setupLocation() {
        locationManager?.delegate = self
        locationManager?.distanceFilter = kCLDistanceFilterNone
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        checkLocationPermissions()
    }

    private func checkLocationPermissions() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                locationManager?.requestWhenInUseAuthorization()
                return
            case .authorizedAlways, .authorizedWhenInUse:
                return
            default:
                break
            }
        }
    }

    private func obtainRestaurants(with coordinates: CLLocation) {
        let coordinate = Coordinate.obtainCoordinate(with: coordinates)
        RestaurantManager.shared.obtainRestaurants(with: coordinate) { [weak self] restaurantsArray, error in
            guard error == nil, let restaurants = restaurantsArray as? [Restaurant]  else {
                return
            }

            self?.restaurants.value = restaurants
        }
    }

}

extension RestaurantViewModel: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else {
            return
        }

        currentLocation = location
        obtainRestaurants(with: location)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorized, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            manager.stopUpdatingLocation()
        }
    }
}
