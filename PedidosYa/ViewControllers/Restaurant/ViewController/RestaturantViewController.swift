//
//  RestaturantViewController.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit
import CoreLocation

class RestaturantViewController: UIViewController {

    // MARK: - IBOutlet

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(withIdentifier: RestaurantTableViewCell.reuseIdentifier)
            tableView.dataSource = self
            tableView.delegate = self
        }
    }

    // MARK: - Properties

    private var viewModel: RestaurantViewModel! {
        didSet {
            viewModel.restaurants.bindAndFire { [weak self] newRestaurants in
                self?.restaurants = newRestaurants
                self?.tableView.reloadData()
            }

            viewModel.presentMapViewController = { [weak self] in
                self?.presentMapViewController()
            }
        }
    }

    private var restaurants: [Restaurant] = []

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RestaurantViewModel(locationManager: CLLocationManager())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidApper()
    }

    // MARK: - IBActions

    @IBAction private func didTapMapButton(_ sender: UIButton) {
        viewModel.didTapMapButton()
    }

    // MARK: - Private Methods

    private func presentMapViewController() {
        present(viewModel.makeMapViewController(), animated: true, completion: nil)
    }

}

extension RestaturantViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableViewCell.reuseIdentifier, for: indexPath) as! RestaurantTableViewCell
        cell.viewModel = RestaurantCellViewModel(restaurant: restaurants[indexPath.row])
        return cell
    }

}

extension RestaturantViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

}
