//
//  RestaurantTableViewCell.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet private weak var restaurantNameLabel: UILabel!

    // MARK: - Properties

    var viewModel: RestaurantCellViewModel! {
        didSet {
            viewModel.name.bindAndFire { [weak self] name in
                self?.restaurantNameLabel.text = name
            }
        }
    }

    // MARK: - Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

}
