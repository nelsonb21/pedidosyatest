//
//  RestaurantCellViewModel.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class RestaurantCellViewModel {

    // MARK: - Properties

    let name: Bindable<String?>

    // MARK: - Life Cycle

    init(restaurant: Restaurant) {
        name = Bindable(restaurant.name)
    }

}
