//
//  MapViewModel.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import CoreLocation

class MapViewModel: NSObject {

    // MARK: - Properties

    let restaurants: Bindable<[Restaurant]> = Bindable([])
    let currentLocation: Bindable<CLLocation?> = Bindable(nil)

    private var locationManager: CLLocationManager?

    // MARK: - Events

    var setCurrentLocationOnMap: (() -> ())?

    // MARK: - Life Cycle
    
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
    }

    // MARK: - Public Methods

    func viewDidApper() {
        setupLocation()
    }

    // MARK: - Private Methods
    
    private func setupLocation() {
        locationManager?.delegate = self
        locationManager?.distanceFilter = kCLDistanceFilterNone
        locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        checkLocationPermissions()
    }

    private func checkLocationPermissions() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                locationManager?.requestWhenInUseAuthorization()
                return
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager?.startUpdatingLocation()
                return
            default:
                break
            }
        }
    }

    private func obtainRestaurants(with coordinates: CLLocation) {
        let coordinate = Coordinate.obtainCoordinate(with: coordinates)
        RestaurantManager.shared.obtainRestaurants(with: coordinate) { [weak self] restaurantsArray, error in
            guard error == nil, let restaurants = restaurantsArray as? [Restaurant]  else {
                return
            }

            self?.restaurants.value = restaurants
        }
    }
    
}


extension MapViewModel: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = manager.location else {
            return
        }

        currentLocation.value = location
        obtainRestaurants(with: location)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorized, .authorizedWhenInUse:
            manager.startUpdatingLocation()
        default:
            manager.stopUpdatingLocation()
        }
    }
}
