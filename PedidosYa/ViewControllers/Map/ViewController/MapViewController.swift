//
//  MapViewController.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    // MARK: - IBOutlet
    
    @IBOutlet private weak var mapView: MKMapView! {
        didSet {
            mapView.showsUserLocation = true
        }
    }

    // MARK: - Properties

    override class var storyboardID: String? {
        return String(describing: self)
    }

    override class var storyboardName: String {
        return "Map"
    }

    var viewModel: MapViewModel! {
        didSet {
            viewModel.restaurants.bindAndFire { [weak self] newRestaurants in
                self?.restaurants = newRestaurants
                self?.addRestaurantsToMap()
            }

            viewModel.currentLocation.bindAndFire { [weak self] newLocation in
                guard let location = newLocation else {
                    return
                }

                self?.mapView.setCenter(location.coordinate, animated: true)
                self?.centerMapOnLocation(location)
            }
        }
    }

    private var restaurants: [Restaurant] = []

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidApper()
    }

    // MARK: - IBActions

    @IBAction private func didTapCloseButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Private Methods

    private func addRestaurantsToMap() {
        restaurants.forEach { restaurant in
            guard let coordinate = restaurant.coordinates else {
                return
            }

            let annotation = MKPointAnnotation()
            annotation.title = restaurant.name
            annotation.coordinate = coordinate
            self.mapView.addAnnotation(annotation)
        }
    }

    private func centerMapOnLocation(_ location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}
