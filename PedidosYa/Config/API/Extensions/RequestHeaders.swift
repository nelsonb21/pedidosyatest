//
//  NSMutableURLRequestHeaders.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

extension URLRequest {
    
    mutating func addHTTPHeaders(_ headers: [String: String]?) {
        guard let headers = headers else { return }
        for (key, value) in headers {
            setValue(value, forHTTPHeaderField: key)
        }
    }
    
}
