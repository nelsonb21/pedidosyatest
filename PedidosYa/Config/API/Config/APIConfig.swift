//
//  APIConfig.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit
import Alamofire

private struct Constants {
    static let authorizationKey = "Authorization"
}

enum APIConfig {
    case token
    case `default`
    
    var source: (host: String, headers: [String: String]?) {
        switch self {
        case .token:
            return (host: "http://stg-api.pedidosya.com/public/v1/", headers: nil)
        case .default:
            return (host: "http://stg-api.pedidosya.com/public/v1/", headers: [Constants.authorizationKey: Credentials.token])
        }
    }
    
}
