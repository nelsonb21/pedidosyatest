//
//  Restaurant.swift
//  PedidosYa
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/24/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import CoreLocation

class Restaurant {

    var id: String
    var name: String
    var coordinates: CLLocationCoordinate2D?

    init(id: String, name: String, coordinates: CLLocationCoordinate2D?) {
        self.id = id
        self.name = name
        self.coordinates = coordinates
    }

    convenience init(data: [String: Any]) {
        let id = data["id"] as? String ?? ""
        let name = data["name"] as? String ?? ""
        let coordinates = data["coordinates"] as? String ?? ""

        self.init(id: id, name: name, coordinates: Coordinate.separate(coordinates: coordinates))
    }

}

struct Coordinate {

    static func separate(coordinates: String) -> CLLocationCoordinate2D? {
        let coordinates = coordinates.components(separatedBy: ",")
        let lat = coordinates.first ?? ""
        let long = coordinates.last ?? ""

        guard let latitude = Double(lat), let longitude = Double(long) else {
            return nil
        }

        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    static func obtainCoordinate(with location: CLLocation) -> String {
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude

        let coordinate = "\(latitude),\(longitude)"
        return coordinate
    }

}
