//
//  Path.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

private struct Constants {
    static let tokenKey = "tokenKey"
    static let clientIdKey = "clientId"
    static let clientSecretKey = "clientSecret"
}

struct Credentials {
    // TODO: - Colocar las respectivas credenciales en cada una de las variables
    static let clientId = ""
    static let clientSecret = ""

    static var token: String {
        set {
            UserDefaults.standard.set(newValue, forKey: Constants.tokenKey)
        }
        get {
            return UserDefaults.standard.string(forKey: Constants.tokenKey) ?? ""
        }
    }

}

enum Path: Router {
    case token
    case restaurant(point: String, country: String)

    var config: APIConfig {
        switch self {
        case .token:
            return .token
        default:
            return .default
        }
    }

    var query: APIQuery {
        switch(self) {
        case .token:
            return APIQuery(httpMethod: .get, path: "tokens", parameters: [Constants.clientIdKey: Credentials.clientId, Constants.clientSecretKey: Credentials.clientSecret])
        case .restaurant(let point, let country):
            return APIQuery(httpMethod: .get, path: "search/restaurants", parameters: ["point": point, "country": country])
        }
    }

}
